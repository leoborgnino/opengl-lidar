#version 120

attribute vec3 position;

uniform mat4 ModelViewProjectionMatrix;

void main() {
  // Proyección de los puntos 3d a la cámara 2D
  gl_Position = ModelViewProjectionMatrix * vec4(position, 1.0);
}
