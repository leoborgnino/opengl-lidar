import numpy as np
import matplotlib.pyplot as plt

text_file = open('../rgbd_image.jpg')
lines = text_file.read()
lines_splitted = lines.split()
floats = [float(x) for x in lines_splitted]
numpy_array = np.array(floats)
numpy_array2 = np.reshape(numpy_array,(512,512))
plt.imshow(numpy_array2,cmap='gray')
plt.show()
